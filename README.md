Bom dia, pessoa!

Nesse exercício iremos abordar o conteúdo apresentado nas aulas 01, 02 e 03 dessa semana.
Estruturem o diagrama entidade-relacionamento considerando todos os conceitos que aprendemos.
Para facilitar utilizem ferramenta online gratuita para desenho dos diagramas Draw.io (https://app.diagrams.net/).
Para a entrega, anexem o arquivo .drawio gerado e também exportem uma imagem .png.

Problema proposto:

A biblioteca RaroAcademy mantém um acervo de vários itens que podem ser livros, CDs e DVDs.

Todos os itens possuem um código único, título e descrição.
Além disso, os livros possuem sumário, ano de publicação, editora e autores.
Os CDs possuem o nome da produtora, ano e músicas. Músicas possuem código único e nome.
Os DVDs possuem o nome da produtora, ano e diretores.

A biblioteca mantém o cadastro dos fornecedores de seus itens com os seguintes dados: código único, nome, endereço e telefones.
A biblioteca mantém também o cadastro de funcionários com os seguintes dados: código único, matrícula única, nome, endereço, telefones, turno e sexo.
Os associados da biblioteca são identificados por um código, nome, endereço e telefones.
Os associados fazem vários empréstimos dos itens em determinada data e devolvem em uma data específica também.

Todo empréstimo é verificado por um funcionário da biblioteca em determinada data e horário, a fim de detectar se o item que está sendo devolvido não se encontra danificado.

Os funcionários são responsáveis pelas estantes onde são armazenados os itens do acervo.
As estantes possuem um número, andar e sala. Um item fica armazenado em uma única estante, mas uma estante pode armazenar vários itens.

Bom trabalho a todos!

# Relacionamentos
### Estante
1. Pode ter **livros** (1 estante mandatório - n livros opcional)
2. Pode ter **cds** (1 estante mandatório - n cds opcional)
3. Pode ter **dvds** (1 estante mandatório - n dvds opcional)
4. Tem que ser administrado por um e apenas um **funcionário** (1 estante mandatório - 1 funcionário mandatório)

### Livro
1. Estão em uma **estante** (n livros opcional - 1 estante mandatório)
2. Podem fazer parte de um **empréstimo** (n livros opcional - 1 empréstimo opcional)
3. Tem um **fornecedor** (n livros opcional - 1 fornecedor mandatório)

### CD
1. Estão em uma **estante** (n cds opcional - 1 estante mandatório)
2. Tem uma ou mais músicas (1 cd mandatório - n músicas mandatório)
3. Tem um **fornecedor** (n cds opcional - 1 fornecedor mandatório)
4. Podem fazer parte de um **empréstimo** (n cds opcional - 1 empréstimo opcional)

### DVD
1. Estão em uma **estante** (n dvds opcional - 1 estante mandatório)
2. Podem fazer parte de um **empréstimo** (n dvds opcional - 1 empréstimo opcional)
3. Tem um **fornecedor** (n dvds opcional - 1 fornecedor mandatório)
4. Pode ter um ou mais diretores (atributo multivalorado e composto)

### EMPRÉSTIMO
1. Tem que ter um e apenas um **associado**(1 empréstimo opcional - 1 associado mandatório)
2. Pode ter um ou mais **cds** (1 empréstimo opcional - n cds opcional)
3. Pode ter um ou mais **dvds** (1 empréstimo opcional - n dvds opcional)
4. Pode ter um ou mais **livros** (1 empréstimo opcional - n livros opcional)
5. Tem que ter uma e apenas uma **verificação** (1 empréstimo mandatório - 1 verificação mandatória)

### VERIFICAÇÃO
1. Tem que estar relacionado a um e apenas um **empréstimo** (1 verificação mandatória - 1 empréstimo mandatório)
2. Tem que ser feito por um e apenas um **funcionário** (1 verificação mandatório - 1 funcionário mandatório )

### FUNCIONÁRIO
1. Tem que administrar uma e apenas uma **estante** (1 funcionario mandatório - 1 estante mandatória)
2. Tem que realizar pelo menos uma **verificação** (1 funcionário mandatório - n verificações)
3. Tem que ter um ou mais de um telefones (atributo multivalorado)
4. Tem que ter um endereço (atributo multivalorado)

### FORNECEDOR
1. Pode fornecer zero ou vários **livros** (1 fornecedor mandatório - n livros opcional)
2. Pode fornecer zero ou vários **cds** (1 fornecedor mandatório - n cds opcional)
3. Pode fornecer zero ou vários **dvds** (1 fornecedor mandatório - n dvds opcional)
4. Tem que ter um ou mais de um telefones (atributo multivalorado)
5. Tem que ter um endereço (atributo multivalorado)

### ASSOCIADO
1. Pode ou não ter um ou mais **empréstimos** (1 associado mandatório - n empréstimos opcional)
2. Pode ter um ou mais de um telefones (atributo multivalorado)
3. Tem que ter um ou mais de um telefones (atributo multivalorado)
4. Tem que ter um endereço (atributo multivalorado)
![image.png](./image.png)
